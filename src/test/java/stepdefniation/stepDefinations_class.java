package stepdefniation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class stepDefinations_class {
	public static WebDriver driver;

	@Given("user navicate the DemoWenshop Applications")
	public void user_navicate_the_demo_wenshop_applications() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://demowebshop.tricentis.com/");
	}

	@When("user click the login button")
	public void user_click_the_login_button() {
		driver.findElement(By.partialLinkText("Log in")).click();

	}

	@When("user enter email  as {string}")
	public void user_enter_email_as(String username) {
		driver.findElement(By.id("Email")).sendKeys(username);

	}

	@When("user enter passwords as {string}")
	public void user_enter_passwords_as(String passwords) {
		driver.findElement(By.id("Password")).sendKeys(passwords);

	}

	@When("user click on the login button")
	public void user_click_on_the_login_button() {
		driver.findElement(By.id("RememberMe")).click();
		driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();

	}

	@Then("the login should be passed in displyed")
	public void the_login_should_be_passed_in_displyed() {

		driver.close();
	}

}
