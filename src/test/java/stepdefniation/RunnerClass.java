package stepdefniation;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

//@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature/Demo.feature",glue = {"stepdefniation"},monochrome = false)

public class RunnerClass extends AbstractTestNGCucumberTests {

}
